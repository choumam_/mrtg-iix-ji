# Selamat datang

Proyek desain ulang tampilan MRTG IIX-JI menggunakan Bootstrap 5

# Lisensi

Proyek disediakan apa adanya tanpa ada jaminan apapun.

# Wiki

Lihat panduan pemasangan MRTG IIX-JI lebih lanjut di [laman wiki](https://gitlab.com/choumam_/mrtg-iix-ji/-/wikis/selamat-datang)
